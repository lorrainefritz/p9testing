package com.dummy.myerp.business.impl.manager;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import com.dummy.myerp.model.bean.comptabilite.*;
import org.junit.Before;
import org.junit.Test;
import com.dummy.myerp.technical.exception.FunctionalException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ComptabiliteManagerImplTest {

    private ComptabiliteManagerImpl comptabiliteManager = new ComptabiliteManagerImpl();
    EcritureComptable ecritureComptableUnderTest;

    @Before
    public void setup() {
        comptabiliteManager = new ComptabiliteManagerImpl();
        ecritureComptableUnderTest = new EcritureComptable();
        JournalComptable journalComptable = new JournalComptable("AC", "Achat");
        ecritureComptableUnderTest.setJournal(journalComptable);
        ecritureComptableUnderTest.setDate(new Date());
        ecritureComptableUnderTest.setLibelle("Libelle");
        ecritureComptableUnderTest.setReference("AC-2021/00001");
    }

    //--------------checkEcritureComptableUnit--------------------------------------------------------------------
    @Test
    public void checkEcritureComptableUnit_ShouldReturnAFunctionnalError_WhenGivenInvaliEntity() {
        EcritureComptable ecritureComptableUnderTest = new EcritureComptable();
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    public void checkEcritureComptableUnit_ShouldReturnAFunctionnalError_WhenGivenInvalidLibelle() {
        EcritureComptable ecritureComptableUnderTest = new EcritureComptable();
        ecritureComptableUnderTest.setLibelle("");
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'écriture comptable ne respecte pas les règles de gestion.");
    }


    @Test
    public void checkEcritureComptableUnit_ShouldThrowAFunctionnalException_WhenEcritureComptableIsNotEquilibree_RG2() {
        //GIVEN WHEN
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, new BigDecimal(123),
                null));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2),
                null, null,
                new BigDecimal(1234)));

        //THEN
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'écriture comptable n'est pas équilibrée.");
    }

    @Test
    public void checkEcritureComptableUnit_ShouldTrowAFunctionnalException_WhenGivenTwoCreditsNoDebit_RG3() throws Exception {

        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, null,
                new BigDecimal(123)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, null,
                new BigDecimal(123)));

        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'écriture comptable doit avoir au moins deux lignes : une ligne au débit et une ligne au crédit.");

    }


    @Test
    public void checkEcritureComptableUnit_ShoulThrowAFunctionnalException_WhenEcritureComptableDebitAndCreditAreNull_RG4() {
        //GIVEN WHEN
        EcritureComptable ecritureComptableUnderTest;
        ecritureComptableUnderTest = new EcritureComptable();
        ecritureComptableUnderTest.setJournal(new JournalComptable("AC", "Achat"));
        ecritureComptableUnderTest.setDate(new Date());
        ecritureComptableUnderTest.setLibelle("Libelle");
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, null,
                null));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2),
                null, null,
                null));

        //THEN
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'écriture comptable doit avoir au moins deux lignes : une ligne au débit et une ligne au crédit.");
    }


    @Test
    public void checkEcritureComptableUnit_ForEcitureComptableReferenceMustMatchJournalComptableCode_ShouldThrowAFunctionnalException_WhenFirstElementDontMatch_RG5() {
        EcritureComptable ecritureComptableUnderTest = new EcritureComptable();
        JournalComptable journalComptable = new JournalComptable("AC", "Achat");
        ecritureComptableUnderTest.setJournal(journalComptable);
        ecritureComptableUnderTest.setDate(new Date());
        ecritureComptableUnderTest.setLibelle("Libelle");
        ecritureComptableUnderTest.setReference("DC-2021/00001");

        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(200.50), null));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(100.50), new BigDecimal(33)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2, "Libelle"), "Libelle", null, new BigDecimal(301)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(40), new BigDecimal(7)));


        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("Les deux premières lettre de la référence : " + ecritureComptableUnderTest.getReference() + " et le code du journal : " + journalComptable.getCode() + " ne correspondent pas");
    }

    @Test
    public void checkEcritureComptableUnit_ForEcritureComptableReferenceMustMatchJournalComptableCode_ShouldThrowAFunctionnalException_WhenDatesDontMatch_RG5() {
        EcritureComptable ecritureComptableUnderTest = new EcritureComptable();
        JournalComptable journalComptable = new JournalComptable("AC", "Achat");
        ecritureComptableUnderTest.setJournal(journalComptable);
        ecritureComptableUnderTest.setDate(new Date());
        ecritureComptableUnderTest.setLibelle("Libelle");
        ecritureComptableUnderTest.setReference("AC-2077/00001");
        String journalCode = ecritureComptableUnderTest.getJournal().getCode();
        String reference = ecritureComptableUnderTest.getReference();
        Calendar calendar = Calendar.getInstance();
        int annee = calendar.get(Calendar.YEAR);
        String[] referenceTesting = reference.split("[-/]");

        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(200.50), null));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(100.50), new BigDecimal(33)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2, "Libelle"), "Libelle", null, new BigDecimal(301)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(40), new BigDecimal(7)));

        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'année : " + referenceTesting[1] + " ne correspond pas à l'année de référence " + annee);
    }


    @Test
    public void checkEcritureComptableUnit_forEcritureComptableReferenceShouldNotBeNull_ShouldThrowAfunctionnalError_RG5() {
        EcritureComptable ecritureComptableUnderTest = new EcritureComptable();
        JournalComptable journalComptable = new JournalComptable("AC", "Achat");
        ecritureComptableUnderTest.setJournal(journalComptable);
        ecritureComptableUnderTest.setDate(new Date());
        ecritureComptableUnderTest.setLibelle("Libelle");
        ecritureComptableUnderTest.setReference(null);
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(200.50), null));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(100.50), new BigDecimal(33)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2, "Libelle"), "Libelle", null, new BigDecimal(301)));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1, "Libelle"), "Libelle", new BigDecimal(40), new BigDecimal(7)));

        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("La référence de l'écriture comptable ne doit pas être nulle");
    }

    @Test
    public void checkEcritureComptableUnit_ShouldThrowAfunctionnalError_WhenGivenABadCreditScale_RG7(){

        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, new BigDecimal(123.001),
                null));
        ecritureComptableUnderTest.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1 ),
                null, null,
                new BigDecimal(123.001)));

        //THEN
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManager.checkEcritureComptableUnit(ecritureComptableUnderTest);
        }).withMessage("L'écriture comptable ne respecte pas les règles de gestion.");
    }

}
