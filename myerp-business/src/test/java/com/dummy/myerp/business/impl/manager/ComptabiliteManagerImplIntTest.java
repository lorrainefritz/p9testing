package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.transaction.TransactionStatus;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ComptabiliteManagerImplIntTest {

    @Mock
    DaoProxy daoProxy;

    @Mock
    ComptabiliteDao comptabiliteDao;

    @Mock
    BusinessProxy businessProxy;

    @Mock
    TransactionManager transactionManager;

    @Mock
    ComptabiliteManagerImpl comptabiliteManagerImpl;

    @Mock
    TransactionStatus transactionStatus;

    EcritureComptable ecritureComptable;

    @Before
    public void setup() {
        comptabiliteManagerImpl = new ComptabiliteManagerImpl();
        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);
        ecritureComptable = new EcritureComptable();
        JournalComptable pJournal = new JournalComptable("AC", "Achat");
        ecritureComptable.setJournal(pJournal);
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setReference("AC-2021/00001");
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, new BigDecimal(213), null));
        ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                null, null, new BigDecimal(213)));
    }


    //--------------AddReference--------------------------------------------------------------------
    @Test

    public void checkAddReference_ShouldReturnNew_WhenGivenANewReference() throws NotFoundException {
        String journalCode = ecritureComptable.getJournal().getCode();
        Date dateOfEcriture = ecritureComptable.getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateOfEcriture);
        int yearOfEcritureOfEC = calendar.get(Calendar.YEAR);


        when(comptabiliteDao.getSequenceEcritureComptableByCodeAndYear(journalCode, yearOfEcritureOfEC)).thenReturn(null);
        comptabiliteManagerImpl.addReference(ecritureComptable);
        verify(daoProxy, times(3)).getComptabiliteDao();
        verify(comptabiliteDao, times(1)).insertSequenceDEcritureComptable(any());
    }

    @Test
    public void checkAddReference_ShouldReturnTheUpdateValue_WhenGivenAnExistingReference() throws NotFoundException {
        JournalComptable journalComptable = new JournalComptable("AC", "Achat");
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2021, 00001, journalComptable);
        String journalCode = ecritureComptable.getJournal().getCode();
        Date dateOfEcriture = ecritureComptable.getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateOfEcriture);
        int yearOfEcritureOfEC = calendar.get(Calendar.YEAR);


        when(comptabiliteDao.getSequenceEcritureComptableByCodeAndYear(journalCode, yearOfEcritureOfEC)).thenReturn(sequenceEcritureComptable);
        comptabiliteManagerImpl.addReference(ecritureComptable);
        verify(daoProxy, times(3)).getComptabiliteDao();
        verify(comptabiliteDao, times(1)).updateSequenceEcritureComptable(any());
    }


    //--------------checkEcritureComptable--------------------------------------------------------------------

    @Test
    public void checkEcritureComptable_ShouldReturnAValidEntity_WhenGivenAValidEntry() throws NotFoundException, FunctionalException {
        ecritureComptable.setId(1);
        JournalComptable journalComptable = ecritureComptable.getJournal();
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2021, 1, journalComptable);
        when(comptabiliteDao.getSequenceEcritureComptableByCodeAndYear(anyString(), anyInt())).thenReturn(sequenceEcritureComptable);
        when(comptabiliteDao.getEcritureComptableByRef("AC-2021/00001")).thenReturn(ecritureComptable);
        comptabiliteManagerImpl.checkEcritureComptable(ecritureComptable);
    }

    @Test
    public void checkEcritureComptable_ShouldReturnAFunctionnalError_WhenGivenAnInvalidEntry_RG5() throws NotFoundException {
        ecritureComptable.setReference("AC-2021/00001");

        JournalComptable journalComptable = ecritureComptable.getJournal();
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2021, 3, journalComptable);

        String journalCode = ecritureComptable.getJournal().getCode();
        Date dateOfEcriture = ecritureComptable.getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateOfEcriture);
        int yearOfEcritureOfEC = calendar.get(Calendar.YEAR);

        given(comptabiliteDao.getSequenceEcritureComptableByCodeAndYear(journalCode, yearOfEcritureOfEC)).willReturn(sequenceEcritureComptable);
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManagerImpl.checkEcritureComptable(ecritureComptable);
        }).withMessage("Les derniers chiffres de la séquence 00001 ne correspondent pas à la sequence 3");
    }

    @Test
    public void checkEcritureComptableContext_ShouldReturnAFunctionnalError_WhenGivenANewRef_RG6() throws NotFoundException {

        given(comptabiliteDao.getEcritureComptableByRef(anyString())).willReturn(ecritureComptable);
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManagerImpl.checkEcritureComptableContext(ecritureComptable);
        }).withMessage("Une autre écriture comptable existe déjà avec la même référence.");
    }

    @Test
    public void checkEcritureComptableContext_ShouldReturnAFunctionnalError_WhenGivenAnExistingRef_RG6() throws NotFoundException {
        ecritureComptable.setId(1);
        EcritureComptable ecritureComptableUnderTest = new EcritureComptable();
        ecritureComptableUnderTest.setId(2);
        when(comptabiliteDao.getEcritureComptableByRef(anyString())).thenReturn(ecritureComptableUnderTest);
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            comptabiliteManagerImpl.checkEcritureComptableContext(ecritureComptable);
        }).withMessage("Une autre écriture comptable existe déjà avec la même référence.");
    }

    //--------------InsertEcritureComptable--------------------------------------------------------------------
    @Test
    public void checkInsertEcritureCompatble_ShouldCallTransactionManager() throws NotFoundException, FunctionalException {
        transactionStatus = transactionManager.beginTransactionMyERP();
        ecritureComptable.setId(1);
        JournalComptable journalComptable = ecritureComptable.getJournal();
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2021, 1, journalComptable);
        when(comptabiliteDao.getEcritureComptableByRef("AC-2021/00001")).thenReturn(ecritureComptable);
        when(comptabiliteDao.getSequenceEcritureComptableByCodeAndYear(anyString(), anyInt())).thenReturn(sequenceEcritureComptable);
        comptabiliteManagerImpl.insertEcritureComptable(ecritureComptable);

        verify(daoProxy, times(3)).getComptabiliteDao();
        verify(transactionManager, times(1)).commitMyERP(transactionStatus);
        verify(transactionManager, times(1)).rollbackMyERP(transactionStatus);
    }

//--------------UpdateEcritureComptable--------------------------------------------------------------------

    @Test
    public void checkUpdateEcritureCompatble_ShouldCallTransactionManager() throws NotFoundException, FunctionalException {
        transactionStatus = transactionManager.beginTransactionMyERP();
        ecritureComptable.setId(1);
        JournalComptable journalComptable = ecritureComptable.getJournal();
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2021, 1, journalComptable);
        when(comptabiliteDao.getEcritureComptableByRef("AC-2021/00001")).thenReturn(ecritureComptable);
        when(comptabiliteDao.getSequenceEcritureComptableByCodeAndYear(anyString(), anyInt())).thenReturn(sequenceEcritureComptable);
        comptabiliteManagerImpl.updateEcritureComptable(ecritureComptable);

        verify(daoProxy, times(3)).getComptabiliteDao();
        verify(transactionManager, times(1)).commitMyERP(transactionStatus);
        verify(transactionManager, times(1)).rollbackMyERP(transactionStatus);
    }
//--------------DelteEcritureComptable--------------------------------------------------------------------

    @Test
    public void checkDeleteEcritureCompatble_ShouldCallTransactionManager() {
        transactionStatus = transactionManager.beginTransactionMyERP();
        ecritureComptable.setId(1);
        comptabiliteManagerImpl.deleteEcritureComptable(ecritureComptable.getId());

        verify(daoProxy, times(1)).getComptabiliteDao();
        verify(transactionManager, times(1)).commitMyERP(transactionStatus);
        verify(transactionManager, times(1)).rollbackMyERP(transactionStatus);
    }


}
