package com.dummy.myerp.business.impl.manager;

import java.math.BigDecimal;
import java.util.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.dummy.myerp.model.bean.comptabilite.*;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.TransactionStatus;
import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;


/**
 * Comptabilite manager implementation.
 */
public class ComptabiliteManagerImpl extends AbstractBusinessManager implements ComptabiliteManager {

    // ==================== Attributs ====================


    // ==================== Constructeurs ====================

    /**
     * Instantiates a new Comptabilite manager.
     */
    public ComptabiliteManagerImpl() {
    }


    // ==================== Getters/Setters ====================
    @Override
    public List<CompteComptable> getListCompteComptable() {
        return getDaoProxy().getComptabiliteDao().getListCompteComptable();
    }


    @Override
    public List<JournalComptable> getListJournalComptable() {
        return getDaoProxy().getComptabiliteDao().getListJournalComptable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        return getDaoProxy().getComptabiliteDao().getListEcritureComptable();
    }

    /**
     * {@inheritDoc}
     */
    // TODO à tester
    @Override
    public synchronized void addReference(EcritureComptable pEcritureComptable) {
        // TODO à implémenter
        // Bien se réferer à la JavaDoc de cette méthode !
        /* Le principe :
                 1.  Remonter depuis la persitance la dernière valeur de la séquence du journal pour l'année de l'écriture
                    (table sequence_ecriture_comptable)
                2.  * S'il n'y a aucun enregistrement pour le journal pour l'année concernée :
                        1. Utiliser le numéro 1.
                    * Sinon :
                        1. Utiliser la dernière valeur + 1
                3.  Mettre à jour la référence de l'écriture avec la référence calculée (RG_Compta_5)
                4.  Enregistrer (insert/update) la valeur de la séquence en persitance
                    (table sequence_ecriture_comptable)
         */


        //--1. on remonte à la dernière valeur de la séquence du journal
        SequenceEcritureComptable vSequenceEcritureComptable = null;

        //--------la date puis l'année de l'ecritureComptable
        Date dateOfEcriture = pEcritureComptable.getDate();
        Calendar calendr = Calendar.getInstance();
        calendr.setTime(dateOfEcriture);
        int yearOfEcritureOfThisEcritureComptable = calendr.get(Calendar.YEAR);
        //-------le code du journal
        String code = pEcritureComptable.getJournal().getCode();
        try {
            vSequenceEcritureComptable = getDaoProxy().getComptabiliteDao().getSequenceEcritureComptableByCodeAndYear(code, yearOfEcritureOfThisEcritureComptable);

        } catch (NotFoundException notFoundException) {
            notFoundException.printStackTrace();
        }
        //--2. s'il n'y a pas d'entrée pour la date on met la dernière valeur à 1 SINON la dernière valeur est utilisée +1
        if (vSequenceEcritureComptable == null) {

            vSequenceEcritureComptable = new SequenceEcritureComptable(yearOfEcritureOfThisEcritureComptable, 1, pEcritureComptable.getJournal());
        } else {
            vSequenceEcritureComptable.setDerniereValeur(vSequenceEcritureComptable.getDerniereValeur() + 1);
        }
        //--3 MAJ de la ref avec la ref calculée
        String vReferenceFormated = String.format("%05d", vSequenceEcritureComptable.getDerniereValeur());

        //--4 enregistrement de la ref
        pEcritureComptable.setReference(vReferenceFormated);
        getDaoProxy().getComptabiliteDao().updateEcritureComptable(pEcritureComptable);

        if (vSequenceEcritureComptable.getDerniereValeur() == 1) {
            getDaoProxy().getComptabiliteDao().insertSequenceDEcritureComptable(vSequenceEcritureComptable);
        } else {
            getDaoProxy().getComptabiliteDao().updateSequenceEcritureComptable(vSequenceEcritureComptable);
        }

    }

    /**
     * {@inheritDoc}
     */
    // TODO à tester
    @Override
    public void checkEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptableUnit(pEcritureComptable);
        this.checkEcritureComptableContext(pEcritureComptable);
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion unitaires,
     * c'est à dire indépendemment du contexte (unicité de la référence, exercie comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    // TODO tests à compléter
    protected void checkEcritureComptableUnit(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== Vérification des contraintes unitaires sur les attributs de l'écriture
        Set<ConstraintViolation<EcritureComptable>> vViolations = getConstraintValidator().validate(pEcritureComptable);
        if (!vViolations.isEmpty()) {
            throw new FunctionalException("L'écriture comptable ne respecte pas les règles de gestion.",
                    new ConstraintViolationException(
                            "L'écriture comptable ne respecte pas les contraintes de validation",
                            vViolations));
        }

        // ===== RG_Compta_3 : une écriture comptable doit avoir au moins 2 lignes d'écriture (1 au débit, 1 au crédit)
        int vNbrCredit = 0;
        int vNbrDebit = 0;
        for (LigneEcritureComptable vLigneEcritureComptable : pEcritureComptable.getListLigneEcriture()) {
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getCredit(),
                    BigDecimal.ZERO)) != 0) {
                vNbrCredit++;
            }
            if (BigDecimal.ZERO.compareTo(ObjectUtils.defaultIfNull(vLigneEcritureComptable.getDebit(),
                    BigDecimal.ZERO)) != 0) {
                vNbrDebit++;
            }
        }
        // On test le nombre de lignes car si l'écriture à une seule ligne
        //      avec un montant au débit et un montant au crédit ce n'est pas valable
        if (pEcritureComptable.getListLigneEcriture().size() < 2
                || vNbrCredit < 1
                || vNbrDebit < 1) {
            throw new FunctionalException(
                    "L'écriture comptable doit avoir au moins deux lignes : une ligne au débit et une ligne au crédit.");
        }
        // ===== RG_Compta_2 : Pour qu'une écriture comptable soit valide, elle doit être équilibrée
        if (!pEcritureComptable.isEquilibree()) {
            throw new FunctionalException("L'écriture comptable n'est pas équilibrée.");
        }


        // TODO ===== RG_Compta_5 : Format et contenu de la référence
        // vérifier que l'année dans la référence correspond bien à la date de l'écriture, idem pour le code journal...
        /*La référence d'une écriture comptable est composée du code du journal dans lequel figure l'écriture suivi de l'année et d'un numéro de séquence (propre à chaque journal) sur 5 chiffres incrémenté automatiquement à chaque écriture.
        Le formatage de la référence est : XX-AAAA/#####.
        Ex : Journal de banque (BQ), écriture au 31/12/2016
                --> BQ-2016/00001*/


        if (pEcritureComptable != null) {
            if (pEcritureComptable.getReference() == null) {
                throw new FunctionalException("La référence de l'écriture comptable ne doit pas être nulle");
            } else {
                Calendar calendar = Calendar.getInstance();
                // pour avoir l'année courante
                int anneeCourante = calendar.get(Calendar.YEAR);
                // pour avoir l'année d'écriture de EcritureComptable
                Calendar caldr = Calendar.getInstance();
                caldr.setTime(pEcritureComptable.getDate());
                int anneeDecriture = caldr.get(Calendar.YEAR);
                //
                String reference = pEcritureComptable.getReference();
                String journalCode = pEcritureComptable.getJournal().getCode();
                String[] referenceTesting = reference.split("[-/]");

                if (!referenceTesting[0].equals(journalCode)) {
                    throw new FunctionalException("Les deux premières lettre de la référence : " + pEcritureComptable.getReference() + " et le code du journal : " + journalCode + " ne correspondent pas");
                }
                if (!referenceTesting[1].equals(String.valueOf(anneeCourante))) {
                    throw new FunctionalException("L'année : " + referenceTesting[1] + " ne correspond pas à l'année de référence " + anneeCourante);
                }

                try {
                    SequenceEcritureComptable vSequenceEcritureComptable = getDaoProxy().getComptabiliteDao().getSequenceEcritureComptableByCodeAndYear(pEcritureComptable.getJournal().getCode(), anneeDecriture);

                    if (!referenceTesting[2].equals(String.format("%05d", vSequenceEcritureComptable.getDerniereValeur()))) {
                        throw new FunctionalException("Les derniers chiffres de la séquence " + referenceTesting[2] + " ne correspondent pas à la sequence " + vSequenceEcritureComptable.getDerniereValeur());
                    }

                } catch (NotFoundException e) {
                    System.err.println("La séquence d'écriture comptable n'existe pas.");
                }
            }
        }
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion liées au contexte
     * (unicité de la référence, année comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    protected void checkEcritureComptableContext(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== RG_Compta_6 : La référence d'une écriture comptable doit être unique
        if (StringUtils.isNoneEmpty(pEcritureComptable.getReference())) {
            try {
                // Recherche d'une écriture ayant la même référence
                EcritureComptable vECRef = getDaoProxy().getComptabiliteDao().getEcritureComptableByRef(
                        pEcritureComptable.getReference());

                // Si l'écriture à vérifier est une nouvelle écriture (id == null),
                // ou si elle ne correspond pas à l'écriture trouvée (id != idECRef),
                // c'est qu'il y a déjà une autre écriture avec la même référence
                if (pEcritureComptable.getId() == null
                        || !pEcritureComptable.getId().equals(vECRef.getId())) {
                    throw new FunctionalException("Une autre écriture comptable existe déjà avec la même référence.");
                }
            } catch (NotFoundException vEx) {
                // Dans ce cas, c'est bon, ça veut dire qu'on n'a aucune autre écriture avec la même référence.
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().insertEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().updateEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteEcritureComptable(Integer pId) {
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().deleteEcritureComptable(pId);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }
}
