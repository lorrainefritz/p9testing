package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Assert;
import org.junit.Test;

public class SequenceEcritureComptableTest {

    @Test
    public void toString_shouldCoutain_aGivenAnnee() {
        //GIVEN WHEN
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(1908, 6);
        //THEN
        Assert.assertTrue(sequenceEcritureComptable.toString().contains("1908"));
    }

    @Test
    public void toString_shouldCoutain_aGivenDerniereValeur() {
        //GIVEN WHEN
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(1903, 38);
        //THEN
        Assert.assertTrue(sequenceEcritureComptable.toString().contains("38"));
    }

}

