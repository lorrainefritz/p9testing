package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Before;
import org.junit.Test;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.Assert;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

public class CompteComptableTest {

    CompteComptable compteComptable;
    List<CompteComptable>listCompte = new ArrayList<>();

    @Before
    public void setup() {
        compteComptable = new CompteComptable();
        listCompte.add(new CompteComptable(1, "Achat1"));
        listCompte.add(new CompteComptable(2, "Achat2"));

    }

    @Test
    public void toStringTest() {
        this.compteComptable = new CompteComptable(listCompte.get(1).getNumero(), listCompte.get(1).getLibelle());
        String expectedString = "CompteComptable{numero=" + listCompte.get(1).getNumero() + ", libelle='" + listCompte.get(1).getLibelle() + "'}";
        String resultString = compteComptable.toString();
        assertThat(expectedString).isEqualTo(resultString);
    }

}
