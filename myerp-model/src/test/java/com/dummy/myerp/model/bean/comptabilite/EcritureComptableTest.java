package com.dummy.myerp.model.bean.comptabilite;

import java.math.BigDecimal;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;

public class EcritureComptableTest {

    private EcritureComptable ecritureComptable;

    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();
        LigneEcritureComptable vRetour = new LigneEcritureComptable(new CompteComptable(pCompteComptableNumero),
                vLibelle,
                vDebit, vCredit);
        return vRetour;
    }


    @Test
    public void isEquilibree_ShouldReturnThatAGivenEcritureComptableIsEquilibree_WhenEcritureComptableIsEquilibree() {

        ecritureComptable = new EcritureComptable();
        ecritureComptable.setLibelle("Equilibrée");
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "200.50", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", "33"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "301"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, "40", "7"));
        Assert.assertTrue(ecritureComptable.toString(), ecritureComptable.isEquilibree());
    }

    @Test
    public void isEquilibree_ShouldReturnThatAGivenEcritureComptableIsNOTEquilibree_WhenEcritureComptableIsNOTEquilibree() {

        ecritureComptable = new EcritureComptable();
        ecritureComptable.setLibelle("Non équilibrée");
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "10", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "20", "1"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "30"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, "1", "2"));
        Assert.assertFalse(ecritureComptable.toString(), ecritureComptable.isEquilibree());

    }


    @Test
    public void getTotalDebit_ShouldReturnTheSumOfDebits() {
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setLibelle("TestDebit");
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "10", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, "30", null));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, "5", "50"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, "5", null));

        BigDecimal totalDebit = new BigDecimal(50);

        assertThat(ecritureComptable.getTotalDebit()).isEqualTo(totalDebit);
    }

    @Test
    public void getTotalCredit_ShouldReturnTheSumOfCredits() {
        ecritureComptable = new EcritureComptable();
        ecritureComptable.setLibelle("TestDebit");
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, null, "10"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(1, null, "10"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, "40", "10"));
        ecritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "10"));

        BigDecimal totalCredit = new BigDecimal(40);

        assertThat(ecritureComptable.getTotalDebit()).isEqualTo(totalCredit);

    }

}
