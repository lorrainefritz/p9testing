package com.dummy.myerp.model.bean.comptabilite;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class JournalComptableTest {

    @Test
    public void getByCode_ShouldReturnAjournalComptable_WhenGivenAValidCode(){
        //GIVEN
        List<JournalComptable> listOfjournalComptableToTest = new ArrayList<>();
        JournalComptable journalComptableA = new JournalComptable("12345","journalComptableA");
        JournalComptable journalComptableB = new JournalComptable("23456","journalComptableB");
        JournalComptable journalComptableC = new JournalComptable("34567","journalComptableC");
        listOfjournalComptableToTest.add(journalComptableA);
        listOfjournalComptableToTest.add(journalComptableB);
        listOfjournalComptableToTest.add(journalComptableC);

        //WHEN
        String givenCode = "34567";
        JournalComptable journalComptableUnderTest = JournalComptable.getByCode(listOfjournalComptableToTest,givenCode);
        //THEN
        assertThat(journalComptableUnderTest).isEqualTo(journalComptableC);
    }
    @Test
    public void toString_ShouldContain_aGivenCode(){
        //GIVEN WHEN
        JournalComptable journalComptableUnderTest = new JournalComptable("12345","journalComptableUnderTest");
        //THEN
        Assert.assertTrue(journalComptableUnderTest.toString().contains("12345"));
    }
    @Test
    public void toString_shouldContain_aGivenLibelle(){
        //GIVEN WHEN
        JournalComptable journalComptableUnderTest = new JournalComptable("12345","journalComptableUnderTest");
        //THEN
        Assert.assertTrue(journalComptableUnderTest.toString().contains("journalComptableUnderTest"));
    }


}
