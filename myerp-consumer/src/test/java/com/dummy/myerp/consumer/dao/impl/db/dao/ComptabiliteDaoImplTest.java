package com.dummy.myerp.consumer.dao.impl.db.dao;

import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.db.AbstractDbConsumer;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import java.util.Date;
import java.util.List;
import static org.assertj.core.api.Assertions.*;


@SpringJUnitConfig(locations = {"classpath:bootstrapContext.xml"})
public class ComptabiliteDaoImplTest extends AbstractDbConsumer {


    @Autowired
    ComptabiliteDao comptabiliteDao;

    //--------------listCompteComptable--------------------------------------------------------------------
    @Test
    public void listCompteComptable_ShouldReturnListIsNotEmpty() {
        List<JournalComptable> listJournalComptable = comptabiliteDao.getListJournalComptable();
        assertThat(listJournalComptable.isEmpty()).isFalse();
    }

    @Test
    public void getListCompteComptable_ShouldReturnListIsNotEmpty() {

        assertThat(comptabiliteDao.getListCompteComptable().isEmpty()).isFalse();
    }

    //--------------EcritureComptable--------------------------------------------------------------------

    @Test
    public void getListEcritureComptable_ShouldReturnListIsNotEmpty() {
        List<EcritureComptable> ecritureComptableList = comptabiliteDao.getListEcritureComptable();
        assertThat(ecritureComptableList.isEmpty()).isFalse();
    }

    @Test
    public void getEcritureComptable_ShouldReturnAnEcritureComptable_WhenGivenAValidId() throws NotFoundException {
        EcritureComptable ecritureComptable = comptabiliteDao.getEcritureComptable(-1);
        assertThat(ecritureComptable).isNotNull();
    }

    @Test
    public void getEcritureComptable_ShouldThrowANotFoundException_WhenGivenAWrongId() throws NotFoundException {
        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
            comptabiliteDao.getEcritureComptable(-77);
        });
    }


    @Test
    public void getEcritureComptableByRef_ShouldReturnAnEcritureComptable_WhenGivenAValidRef() throws NotFoundException {
        EcritureComptable ecritureComptable = comptabiliteDao.getEcritureComptableByRef("AC-2016/00001");
        assertThat(ecritureComptable).isNotNull();
    }

    @Test
    public void getEcritureComptableByRef_ShouldReturnAnException_WhenGivenAWrongRef() {
        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
            comptabiliteDao.getEcritureComptableByRef("AC-2077/00001");
        });
    }

    @Test
    public void insertEcritureComptable_ShouldReturnEcritureComptable_WhenGivenAValidEntity() {
        //GIVEN
        EcritureComptable ecritureComptable = new EcritureComptable();
        JournalComptable journal = new JournalComptable();
        journal.setCode("AC");
        journal.setLibelle("Achat");
        ecritureComptable.setJournal(journal);
        ecritureComptable.setDate(new Date());
        ecritureComptable.setLibelle("Achat");
        ecritureComptable.setReference("AC-2021/00001");


        //WHEN
        comptabiliteDao.insertEcritureComptable(ecritureComptable);

        //THEN
        assertThat(ecritureComptable.getId()).isNotNull();

        //CLEAN
        comptabiliteDao.deleteEcritureComptable(ecritureComptable.getId());
    }


    @Test
    public void insertEcritureComptable_ShouldReturnAnException_WhenGivenAWrongEntity() {
        EcritureComptable ecritureComptable = new EcritureComptable();
        JournalComptable journalComptable = new JournalComptable();
        ecritureComptable.setJournal(journalComptable);
        assertThatExceptionOfType(Exception.class).isThrownBy(() -> {
            comptabiliteDao.insertEcritureComptable(ecritureComptable);
        });
    }

    @Test
    public void updateEcritureComptable_ShouldReturnAnEcritureComptable_WhenGivenANewValidEntity() throws NotFoundException {
        EcritureComptable ecritureComptable = comptabiliteDao.getEcritureComptable(-1);
        ecritureComptable.setLibelle("TESTING_REPO_UPDATE_EC");
        comptabiliteDao.updateEcritureComptable(ecritureComptable);
        EcritureComptable ecritureComptableUnderTest = comptabiliteDao.getEcritureComptable(-1);
        assertThat(ecritureComptableUnderTest.getLibelle()).isEqualTo("TESTING_REPO_UPDATE_EC");
    }
    //--------------SequenceEcritureComptable--------------------------------------------------------------------

    @Test
    void GetSequenceEcritureComptable_ShouldReturnASequenceOfEcritureComptable_WhenGivenAValidValue() throws NotFoundException {
        SequenceEcritureComptable sequenceEcritureComptable = comptabiliteDao.getSequenceEcritureComptableByCodeAndYear("AC", 2016);
        assertThat(sequenceEcritureComptable).isNotNull();
    }

    @Test
    void GetSequenceEcritureComptable_ShouldThrowAnExceptionNotFound_WhenGivenAnInvalidValue() throws NotFoundException {

        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
            comptabiliteDao.getSequenceEcritureComptableByCodeAndYear("TEST",2077);
        });
    }
    @Test
    public void insertSequenceEcritureComptable_ShouldReturnAnException_WhenGivenAWrongEntity() {
        JournalComptable journalComptable = new JournalComptable();
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(2021,88,journalComptable);
        assertThatExceptionOfType(Exception.class).isThrownBy(() -> {
            comptabiliteDao.insertSequenceDEcritureComptable(sequenceEcritureComptable);
        });
    }

    @Test
    public void updateSequenceEcritureComptable_ShouldReturnAnException_WhenGivenANullEntity() {
        SequenceEcritureComptable sequenceEcritureComptable = null;
        assertThatExceptionOfType(Exception.class).isThrownBy(() -> {
                    comptabiliteDao.updateSequenceEcritureComptable(sequenceEcritureComptable);});
    }
}